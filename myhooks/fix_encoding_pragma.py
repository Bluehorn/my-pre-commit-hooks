# -*- encoding: utf-8 -*-

import argparse
import contextlib
import itertools
import os
import re
import tempfile
import sys


# Source: https://www.python.org/dev/peps/pep-0263/
# That PEP defines how the encoding of python files is configured.
PEP263_REGEX = br"^[ \t\f]*#.*?coding[:=][ \t]*([-_.a-zA-Z0-9]+)"

# The encoding line to add.
ENCODING_LINE = b"# -*- encoding: utf-8 -*-\n"


def main():
    parser = argparse.ArgumentParser("Fix the encoding pragma of python files")
    parser.add_argument("filenames", nargs="*", help="Filenames to fix")
    args = parser.parse_args()

    files_to_fix = [f for f in args.filenames if not has_valid_encoding(f)]

    for filename in files_to_fix:
        print("Adding encoding pragma to {0}.".format(filename))
        add_encoding(filename)

    sys.exit(1 if files_to_fix else 0)


def has_valid_encoding(filename):
    with open(filename, "rb") as stream:
        for line in itertools.islice(stream, 2):
            m = re.match(PEP263_REGEX, line)
            if m:
                encoding = m.group(1)
                break
        else:
            encoding = None

    return encoding in (b"utf-8", b"UTF-8") or is_empty_source(filename)


def is_empty_source(filename):
    with open(filename, "rb") as stream:
        return not any(l.strip() for l in stream)


def add_encoding(filename):
    with rewrite_stream(filename) as target, open(filename, "rb") as source:
        first_line = next(source)
        if first_line.startswith(b"#!"):
            target.write(first_line)
            target.write(ENCODING_LINE)
        else:
            target.write(ENCODING_LINE)
            target.write(first_line)

        for line in source:
            target.write(line)


@contextlib.contextmanager
def rewrite_stream(filename):
    temp = tempfile.NamedTemporaryFile(
        dir=os.path.dirname(filename),
        prefix=os.path.basename(filename)[0],
        delete=False,
    )
    try:
        yield temp
    except BaseException:
        if temp is not None:
            os.remove(temp.name)
        raise
    else:
        os.rename(temp.name, filename)


if __name__ == "__main__":
    main()
