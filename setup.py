# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup


setup(
    name="Torsten's hooks for pre-commit",
    description="Contains hook(s) for pre-commit, see http://pre-commit.com",
    author="Torsten Landschoff <torsten@landschoff.net>",
    version="0.1.0",

    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],

    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "fix-encoding-pragma = myhooks.fix_encoding_pragma:main",
        ],
    },
)
